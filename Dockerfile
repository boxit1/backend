FROM node:alpine

RUN apk add --update docker openrc docker-compose
RUN rc-update add docker boot
RUN addgroup node docker

WORKDIR /usr/src/app

COPY package.json .
RUN npm install

COPY . .

EXPOSE ${DEV_APP_PORT}

#CMD ["node", "app.js"]
CMD ["npm", "start"]

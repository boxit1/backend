require("dotenv").config();

module.exports = {
  development: {
    port: parseInt(process.env.DB_PORT),
    database: process.env.DB_NAME,
    password: process.env.DB_PASS,
    username: process.env.DB_USER,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    logging: process.env.DB_LOGGING == "true" ? console.log : false,
    migrationStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    migrationStoragePath: process.env.DB_MIGRATION,
    migrationStorageTableName: process.env.DB_MIGRATION,
    seederStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    seederStoragePath: process.env.DB_SEEDER,
    seederStorageTableName: process.env.DB_SEEDER
  },
  test: {
    port: parseInt(process.env.DB_PORT),
    database: process.env.DB_NAME,
    password: process.env.DB_PASS,
    username: process.env.DB_USER,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    logging: process.env.DB_LOGGING == "true" ? console.log : false,
    migrationStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    migrationStoragePath: process.env.DB_MIGRATION,
    migrationStorageTableName: process.env.DB_MIGRATION,
    seederStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    seederStoragePath: process.env.DB_SEEDER,
    seederStorageTableName: process.env.DB_SEEDER
  },
  production: {
    port: parseInt(process.env.DB_PORT),
    database: process.env.DB_NAME,
    password: process.env.DB_PASS,
    username: process.env.DB_USER,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    logging: process.env.DB_LOGGING == "true" ? console.log : false,
    migrationStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    migrationStoragePath: process.env.DB_MIGRATION,
    migrationStorageTableName: process.env.DB_MIGRATION,
    seederStorage: process.env.DB_MIGRATION_STORAGE_TYPE,
    seederStoragePath: process.env.DB_SEEDER,
    seederStorageTableName: process.env.DB_SEEDER
  }
};

"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Roles",
      [
        {
          roleName: "admin",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          roleName: "user",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          roleName: "guest",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Roles", null, {});
  }
};

"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          firstName: "John",
          lastName: "Doe",
          email: "CbqVgSIO35Dj0YYtu1C5ntovnaJ+Qttx1gPOMkim8fM=", //teste@teste.com
          passwordHash:
            "$2b$10$i.jexEBLkwdpm7jCim/bkeddcUqmXnzjmBM1XEo9jz6luxjg5SjfK", // 12345
          accountStatus: "active",
          role: "admin",
          lastLogin: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};

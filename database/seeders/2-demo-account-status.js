"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "AccountStatus",
      [
        {
          statusName: "active",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          statusName: "verifying",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          statusName: "blocked",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("AccountStatus", null, {});
  }
};

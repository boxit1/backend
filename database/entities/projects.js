"use strict";

const Project = require("../../utils/Project");

module.exports = (sequelize, DataTypes) => {
  const Projects = sequelize.define(
    "Projects",
    {
      uuid: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      name: DataTypes.STRING,
    },
    {}
  );
  Projects.associate = function (models) {
    Projects.belongsToMany(models.Users, {
      through: "UsersProjects",
      foreignKey: "project",
      as: "users",
    });
  };
  return Projects;
};

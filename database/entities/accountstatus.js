"use strict";
module.exports = (sequelize, DataTypes) => {
  const AccountStatus = sequelize.define(
    "AccountStatus",
    {
      statusName: {
        type: DataTypes.STRING,
        primaryKey: true
      }
    },
    {
      tableName: "AccountStatus"
    }
  );
  AccountStatus.associate = function(models) {};
  return AccountStatus;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const Roles = sequelize.define(
    "Roles",
    {
      roleName: {
        type: DataTypes.STRING,
        primaryKey: true
      }
    },
    {}
  );
  Roles.associate = function(models) {};
  return Roles;
};

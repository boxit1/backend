"use strict";

const Project = require("../../utils/Project");

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    "Users",
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      passwordHash: DataTypes.STRING,
      lastLogin: DataTypes.DATE,
    },
    {}
  );
  Users.associate = function (models) {
    Users.belongsTo(models.Roles, {
      foreignKey: "role",
    });
    Users.belongsTo(models.AccountStatus, {
      foreignKey: "accountStatus",
    });
    Users.belongsToMany(models.Projects, {
      through: "UsersProjects",
      foreignKey: "user",
      as: "projects",
    });
  };
  return Users;
};

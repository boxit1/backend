"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("UsersProjects", {
      project: {
        allowNull: false,
        primaryKey: true,
        references: {
          model: "Projects",
          key: "uuid",
        },
        onDelete: "CASCADE",
        type: Sequelize.STRING,
      },
      user: {
        allowNull: false,
        primaryKey: true,
        references: {
          model: "Users",
          key: "email",
        },
        onDelete: "CASCADE",
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("UsersProjects");
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Users", {
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      passwordHash: {
        type: Sequelize.STRING
      },
      accountStatus: {
        type: Sequelize.STRING,
        onDelete: "NO ACTION",
        references: {
          model: "AccountStatus",
          key: "statusName"
        }
      },
      role: {
        type: Sequelize.STRING,
        onDelete: "NO ACTION",
        references: {
          model: "Roles",
          key: "roleName"
        }
      },
      lastLogin: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Users");
  }
};

const config = require("./config/config");
const Logger = require("./utils/logger");
const express = require("express");
const RequestHandler = require("./utils/RequestHandler");
const routes = require("./router");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocs = require("./utils/swagger");
const cors = require("cors")

const logger = new Logger();
const app = express();

console.log(`${config.app.frontend_url}`);
app.use(
  cors({
    origin: `${config.app.frontend_url}`, // restrict calls to those this address
    //origin: "http://localhost:3000", // restrict calls to those this address
    methods: "POST,GET,DELETE" // only allow POST requests
  })
);
app.set("db", require("./database/entities"));
app.use(bodyParser.json());
app.use("/", require("./router"));
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.listen(config.app.port, () => {
  logger.log(`Listening on port ${config.app.port}`, "info");
  logger.log(`Check the api documentation on http://localhost:${config.app.port}/api-docs`, "info");
});

module.exports = app;

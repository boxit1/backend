const chai = require("chai");
const chaiHttp = require("chai-http");
const chaiJson = require("chai-json");
const chaiDateString = require("chai-date-string");
const BaseController = require("../../controllers/BaseController");
const app = require("../../app");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const config = require("../../config/config");
config.logger.info = false;
config.logger.error = false;
config.logger.warn = false;

chai.use(chaiHttp);
chai.use(chaiJson);
const expect = chai.expect;
const assert = chai.assert;

const expected = {
  firstName: "John",
  lastName: "Doe",
  email: "teste@teste.com",
  passwordHash: "$2b$10$ZVPoZXS5Hgfk6WKW26YuKO4k6KmqcmrLkSBPMbQYtO5zGBOR1JyHi",
  accountStatus: "active",
  role: "admin",
  lastLogin: new Date(2020, 1, 22, 10, 37, 30),
  createdAt: new Date(2020, 1, 22, 10, 37, 30),
  updatedAt: new Date(2020, 1, 22, 10, 37, 30)
};

const payload = _.omit(expected, [
  "createdAt",
  "updatedAt",
  "lastLogin",
  "passwordHash",
  "accountStatus",
  "role"
]);
const token = jwt.sign({ payload }, config.auth.jwt_secret, {
  expiresIn: config.auth.jwt_expires_in,
  algorithm: config.auth.algorithm
});

describe("User", () => {
  describe("getByPk", () => {
    it("Valid User pk", done => {
      chai
        .request(app)
        .get("/api/users/".concat(expected.email))
        .set("Authorization", `Bearer ${token}`)
        .send()
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.have.property("body");

          expect(res.body).to.have.property("data");
          expect(res.body).to.have.property("type");
          expect(res.body).to.have.property("message");

          expect(res.body.data).to.have.property("result");

          expect(res.body.data.result).to.have.property("firstName");
          expect(res.body.data.result.firstName).to.be.a("string");

          expect(res.body.data.result).to.have.property("lastName");
          expect(res.body.data.result.lastName).to.be.a("string");

          expect(res.body.data.result).to.have.property("email");
          expect(res.body.data.result.email).to.be.a("string");
          expect(res.body.data.result.email).to.equal(expected.email);

          expect(res.body.data.result).to.have.property("passwordHash");
          expect(res.body.data.result.passwordHash).to.be.a("string");

          expect(res.body.data.result).to.have.property("lastLogin");

          expect(res.body.data.result).to.have.property("createdAt");

          expect(res.body.data.result).to.have.property("updatedAt");

          expect(res.body.data.result).to.have.property("role");
          expect(res.body.data.result.role).to.be.a("string");

          expect(res.body.data.result).to.have.property("Role");
          expect(res.body.data.result.Role).to.have.property("roleName");
          expect(res.body.data.result.Role.roleName).to.equal(
            res.body.data.result.role
          );

          expect(res.body.data.result).to.have.property("accountStatus");
          expect(res.body.data.result.accountStatus).to.be.a("string");

          expect(res.body.data.result).to.have.property("AccountStatus");
          expect(res.body.data.result.AccountStatus).to.have.property(
            "statusName"
          );
          expect(res.body.data.result.AccountStatus.statusName).to.equal(
            res.body.data.result.accountStatus
          );
          done();
        });
    });

    it("No header", done => {
      chai
        .request(app)
        .get("/api/users/".concat(expected.email))
        .send()
        .end((err, res) => {
          expect(res.body).to.have.property("type");
          expect(res.body.type).to.be.a("string");
          expect(res.body.type).to.equal("error");

          expect(res.body).to.have.property("message");
          expect(res.body.message).to.be.a("string");

          expect(res.body).to.have.property("error");
          expect(res.body.error).to.have.property("status");
          expect(res.body.error.status).to.equal(401);
          expect(res.body.error).to.have.property("errorType");
          expect(res.body.error.errorType).to.equal("Unauthorized");
          done();
        });
    });
  });
});

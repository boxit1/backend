# Boxit Backend

## Requirements

- [NodeJs](https://nodejs.org)
- [NPM](https://www.npmjs.com/) (usually installed with NodeJs)
- [Docker](https://www.docker.com/)
- RDBMS - supported: [MySQL](https://www.mysql.com/),[ MariaDB](https://mariadb.org/),[ PostgreSQL](https://www.postgresql.org/) and [MsSQL](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)

## Instructions

### 1. Create a `.env` file like the example below

```console
# App
DEV_APP_PORT=5000
APP_NAME=Boxit_Backend
NODE_ENV=development
SINGLE_USER=false
FRONTEND_URL=http://localhost:3000

# Logger
LOG_INFO=true
LOG_ERROR=true
LOG_WARN=true

#SSH
SSH_PASSWORD=Boxit

# Database
DB_PORT=3306
DB_NAME=Boxit_DB
DB_PASS=password123
DB_USER=boxit_user
DB_HOST=127.0.0.1
DB_DIALECT=mysql #options: mysql, mariadb, postgres, mssql
DB_LOGGING=false
DB_MIGRATION_STORAGE_TYPE=none #options: none, json, sequelize
DB_MIGRATION=migrations.json
DB_SEEDER_STORAGE_TYPE=none #options: none, json, sequelize
DB_SEEDER=seeders.json

# Authentication
JWT_SECRET=this is my secret phrase
JWT_EXPIRES_IN=1d
JWT_ALGORITHM=HS512
SALT_ROUND=10
REFRESH_TOKEN_SECRET=this is another secret phrase
REFRESH_TOKEN_EXPIRES_IN=2d
EMAIL_TOKEN_SECRET=my secret email phrase
EMAIL_TOKEN_EXPIRES_IN=1h

# Email
EMAIL_SERVICE=Gmail
EMAIL_USER=boxit.supmail@gmail.com
EMAIL_PASSWORD=BoxItPW63
EMAIL_NAME=Boxit non-reply
```

## 2. Using Docker containers (only works with mysql databases)

## 2.1 first time 
### 2.1.1 Make the Data base folder
```console
mkdir db_data
```

### 2.1.2 change docker.sock permissions

```console
sudo chmod 666 /var/run/docker.sock
```

### 2.1.3 make sure the start script on package.json is as follows:
```json
"start": "npm run rebuild_db && node app.js",
```

### 2.1.4 run with docker-compose

```console
docker-compose up
```
### 2.1.5 stop the backend

```console
CTRL + C
or
docker-compose down
```

This processes was made to create the data base and populate the ```/db_data``` folder

## 2.2 Second time
### 2.2.1 change the start script on package.json to:
```json
"start": "node app.js",
```
### 2.2.2 run with docker-compose

```console
docker-compose up
or
docker-compose up -d
```

From now on you can always start the backend with ```docker-compose up```

### 3. Without Docker containers

### 3.1. Install packages

```console
npm install
```

### 3.2. Build database

```console
npm run build_db
```

### 3.3. Start server

```console
npm run start
```

## API documentation

After starting the server you can check and try the API supported requests on the following url: [http://localhost:5000/api-docs](http://localhost:5000/api-docs)

## Useful commands

| Command              | Behavior                            |
| :------------------- | :---------------------------------- |
| npm run dev          | Start server with nodemon           |
| npm run create_db    | Only create database without tables |
| npm run drop_db      | Drops database                      |
| npm run migrate      | Creates tables on the database      |
| npm run undo_migrate | Undo table creation                 |
| npm run seed         | Insert data on the database tables  |
| npm run undo_seed    | Undo data insertion                 |
| npm run rebuild_db   | Drop database and rebuild it        |
| npm run test         | Run tests                           |

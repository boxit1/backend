const router = require("express").Router();
const AuthController = require("../../controllers/AuthController");
const auth = require("../../utils/auth");

/**
 * @swagger
 * /signUp:
 *  post:
 *    description: Creates a new user and sends an email to verify account
 *    tags:
 *      - Authentication
 *    parameters:
 *      - name: body
 *        in: body
 *        required: true
 *        schema:
 *          type: object
 *          required:
 *            - firstName
 *            - lastName
 *            - email
 *            - password
 *            - role
 *          properties:
 *            firstName:
 *              type: string
 *            lastName:
 *              type: string
 *            email:
 *              type: string
 *            password:
 *              type: string
 *            role:
 *              type: string
 *    responses:
 *      '201':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post("/signUp", AuthController.signUp);


/**
 * @swagger
 * /verifyAccount/{token}:
 *  post:
 *    description: Activates user account
 *    tags:
 *      - Authentication
 *    parameters:
 *      - in: path
 *        name: token
 *        required: true
 *        type: string
 *    responses:
 *      '201':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post(
  "/verifyAccount/:token",
  auth.verifyParamsToken,
  AuthController.verifyAccount
);

/**
 * @swagger
 * /login:
 *  post:
 *    description: Authenticates an user
 *    tags:
 *      - Authentication
 *    parameters:
 *      - name: body
 *        in: body
 *        required: true
 *        schema:
 *          type: object
 *          required:
 *            - email
 *            - password
 *          properties:
 *            email:
 *              type: string
 *            password:
 *              type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                token:
 *                  type: string
 *                refreshToken:
 *                  type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post("/login", AuthController.login);

/**
 * @swagger
 * /logout:
 *  post:
 *    description: End user session
 *    tags:
 *      - Authentication
 *    security:
 *      - Bearer: []
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post("/logout", auth.isAuthenticated, AuthController.logOut);

/**
 * @swagger
 * /refreshToken:
 *  post:
 *    description: Refresh the user token
 *    tags:
 *      - Authentication
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - name: body
 *        in: body
 *        required: true
 *        schema:
 *          type: object
 *          required:
 *            - refreshToken
 *          properties:
 *            refreshToken:
 *              type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                token:
 *                  type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post("/refreshToken", /*auth.isAuthenticated,*/ AuthController.refreshToken);

module.exports = router;

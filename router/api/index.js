const router = require("express").Router();

/**
 * @swagger
 * definitions:
 *   success:
 *     type: object
 *     properties:
 *       type:
 *         type: string
 *       message:
 *         type: string
 *       data:
 *         type: object
 *   error:
 *       type: object
 *       properties:
 *         type:
 *           type: string
 *         message:
 *           type: string
 *         error:
 *           type: object
 *           properties:
 *             status:
 *               type: string
 *             errorType:
 *               type: string
 */
router.use("/users", require("./UsersRouter"));
router.use("/", require("./Root"));
router.use("/", require("./AuthRouter"));
router.use("/projects", require("./ProjectsRouter"));

module.exports = router;

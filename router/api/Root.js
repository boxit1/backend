const router = require("express").Router();
const config = require("../../config/config");

/**
 * @swagger
 * /:
 *  get:
 *    description: Checks if backend is running
 *    tags:
 *      - Root
 *    responses:
 *      '201':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.get("/", function (req, res) {
    res.status(200).json({
        type: "success",
        message: "Backend is Running",
        data: {
            singeUser: config.app.single_user
        }
    });
});

module.exports = router;

const router = require("express").Router();
const UsersController = require("../../controllers/UsersController");
const auth = require("../../utils/auth");

/**
 * @swagger
 * /users/{email}:
 *  get:
 *    description: Creates a new user and sends an email to verify account
 *    tags:
 *      - Users
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: path
 *        name: email
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: object
 *                  properties:
 *                    firstName:
 *                      type: string
 *                    lastName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    passwordHash:
 *                       type: string
 *                    lastLogin:
 *                      type: string
 *                    createdAt:
 *                      type: string
 *                    updatedAt:
 *                      type: string
 *                    role:
 *                      type: string
 *                    accountStatus:
 *                      type: string
 *                    Role:
 *                      type: object
 *                      properties:
 *                        roleName:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                        updatedAt:
 *                          type: string
 *                    AccountStatus:
 *                      type: object
 *                      properties:
 *                        statusName:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                        updatedAt:
 *                          type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.get("/:email", auth.isAuthenticated, UsersController.getUserByPk);


/**
 * @swagger
 * /users/{email}:
 *  delete:
 *    description: Creates a new user and sends an email to verify account
 *    tags:
 *      - Users
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: path
 *        name: email
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: integer
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.delete("/:email", auth.isAuthenticated, UsersController.deleteByPk);

module.exports = router;

const router = require("express").Router();
const ProjectsController = require("../../controllers/ProjectsController");
const auth = require("../../utils/auth");

/**
 * @swagger
 * /projects:
 *  get:
 *    description: Returns a list of projects
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: object
 *                  properties:
 *                    projects:
 *                      type: array
 *                      items:
 *                        type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.get("/", auth.isAuthenticated, ProjectsController.getProjects);

/**
 * @swagger
 * /projects/add:
 *  post:
 *    description: Imports an user project
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: formData
 *        name: project
 *        type: file
 *        required: true
 *      - in: formData
 *        name: instances
 *        type: array
 *        required: true
 *        items:
 *          type: object
 *          schema:
 *            type: object
 *            required:
 *              - name
 *              - fileName
 *              - arguments
 *            properties:
 *              name:
 *                type: string
 *              fileName:
 *                type: string
 *              arguments:
 *                type: array
 *                items:
 *                  type: string
 *              dependsOn:
 *                  type: array
 *                  items:
 *                    type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post("/add", auth.isAuthenticated, ProjectsController.addProject);

/**
 * @swagger
 * /projects/update/{uuid}:
 *  post:
 *    description: Imports an user project
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: path
 *        name: uuid
 *        required: true
 *        type: string
 *      - in: formData
 *        name: project
 *        type: file
 *        required: true
 *      - in: formData
 *        name: instances
 *        type: array
 *        required: true
 *        items:
 *          type: object
 *          schema:
 *            type: object
 *            required:
 *              - name
 *              - fileName
 *              - arguments
 *            properties:
 *              name:
 *                type: string
 *              fileName:
 *                type: string
 *              arguments:
 *                type: array
 *                items:
 *                  type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: null
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.post(
  "/update/:uuid",
  auth.isAuthenticated,
  ProjectsController.addProject
);

/**
 * @swagger
 * /projects/{uuid}:
 *  delete:
 *    description: Deletes a project
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    Bearer:
 *    parameters:
 *      - in: path
 *        name: uuid
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: object
 *                  properties:
 *                    projects:
 *                      type: array
 *                      items:
 *                        type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.delete("/:uuid", auth.isAuthenticated, ProjectsController.deleteProject);

/**
 * @swagger
 * /projects/send/{uuid}:
 *  get:
 *    description: Download the project with the uuid in zip format
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    Bearer:
 *    parameters:
 *      - in: path
 *        name: uuid
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        description: the file in string_binary,
 *        content:
 *          application/zip:
 *            schema:
 *              "$ref": "#/components/schemas/string_binary"
 */
router.get("/send/:uuid", auth.isAuthenticated, ProjectsController.sendProject);


/**
 * @swagger
 * /projects/state/{uuid}:
 *  get:
 *    description: Returns the info of the given uuid container or if not provided the info of all the users containers. All from docker itself
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    Bearer:
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                projects:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      projectUUID:
 *                        type: string
 *                      instances:
 *                        type: array
 *                        items:
 *                          type: object
 *                          properties:
 *                            id:
 *                              type: string
 *                            name:
 *                              type: string
 *                            created:
 *                              type: string
 *                            state:
 *                              type: string
 *                            ip:
 *                              type: string
 *                              
 *                          
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
router.get("/state/:uuid?", auth.isAuthenticated, ProjectsController.getProjectsState);

/**
 * @swagger
 * /projects/up/{uuid}:
 *  post:
 *    description: starts a project
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: path
 *        name: uuid
 *        required: true
 *        type: string
 *      
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: object
 *                  properties:
 *                    uuid: 
 *                      type: string
 *                    name: 
 *                      type: string
 *                    createdAt: 
 *                      type: string
 *                    updatedAt: 
 *                      type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
 router.post(
  "/up/:uuid",
  auth.isAuthenticated,
  ProjectsController.up
);

/**
 * @swagger
 * /projects/down/{uuid}:
 *  post:
 *    description: stops a project
 *    tags:
 *      - Projects
 *    security:
 *      - Bearer: []
 *    parameters:
 *      - in: path
 *        name: uuid
 *        required: true
 *        type: string
 *      
 *    responses:
 *      '200':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/success'
 *          properties:
 *            data:
 *              type: object
 *              properties:
 *                result:
 *                  type: object
 *                  properties:
 *                    uuid: 
 *                      type: string
 *                    name: 
 *                      type: string
 *                    createdAt: 
 *                      type: string
 *                    updatedAt: 
 *                      type: string
 *      'error':
 *        schema:
 *          allOf:
 *            - $ref: '#/definitions/error'
 */
 router.post(
  "/down/:uuid",
  auth.isAuthenticated,
  ProjectsController.down
);

/**
* @swagger
*  /projects/instance/down/{id}:
*  post:
*    description: stops a instance (id -> 64 chars)
*    tags:
*      - Projects
*    security:
*      - Bearer: []
*    parameters:
*      - in: path
*        name: id
*        required: true
*        type: string
*      
*    responses:
*      '200':
*        schema:
*          allOf:
*            - $ref: '#/definitions/success'
*          properties:
*            data:
*              type: object
*              properties:
*                result:
*                  type: object
*                  properties:
*                    uuid: 
*                      type: string
*                    name: 
*                      type: string
*                    createdAt: 
*                      type: string
*                    updatedAt: 
*                      type: string
*      'error':
*        schema:
*          allOf:
*            - $ref: '#/definitions/error'
*/
router.post(
  "/instance/down/:id",
  auth.isAuthenticated,
  ProjectsController.instanceDown
);

/**
* @swagger
*  /projects/instance/up/{id}:
*  post:
*    description: starts a instance (id -> 64 chars)
*    tags:
*      - Projects
*    security:
*      - Bearer: []
*    parameters:
*      - in: path
*        name: id
*        required: true
*        type: string
*      
*    responses:
*      '200':
*        schema:
*          allOf:
*            - $ref: '#/definitions/success'
*          properties:
*            data:
*              type: object
*              properties:
*                result:
*                  type: object
*                  properties:
*                    uuid: 
*                      type: string
*                    name: 
*                      type: string
*                    createdAt: 
*                      type: string
*                    updatedAt: 
*                      type: string
*      'error':
*        schema:
*          allOf:
*            - $ref: '#/definitions/error'
*/
router.post(
  "/instance/up/:id",
  auth.isAuthenticated,
  ProjectsController.instanceUp
);

module.exports = router;

const Docker = require("../utils/Docker");
const DockerCompose = require("../utils/DockerCompose");
const path = require("path");
const fs = require("fs-extra");
const RequestHandler = require("./RequestHandler");
const Logger = require("./logger");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);
const _ = require("lodash");
const config = require("../config/config");

class Project {
  constructor(name, directory) {
    this.name = name;
    this.directory = directory;
    this.dockerCompose = new DockerCompose("2.2");
  }

  getState() {
    let states = [];
    for (var service in this.dockerCompose.services) {
      states.push(
        this.dockerCompose.state(
          this.dockerCompose.services[service].container_name
        )
      );
    }
    return states;
  }

  load() {
    this.dockerCompose.loadFile(
      path.resolve(this.directory, "docker-compose.yaml")
    );
  }

  build(instances) {
    this.dockerCompose.addNetworks(this.name);
    const images = new Set([]);

    instances.forEach((instance) => {
      const instanceSrcPath = path.resolve(this.directory, instance.fileName);
      const instanceDirectory = path.resolve(
        this.directory,
        path.basename(instanceSrcPath).replace(".", "_")
      );

      const instancePath = path.resolve(
        instanceDirectory,
        path.basename(instance.fileName)
      );

      this.dockerCompose.addService(
        this.name,
        instance.name,
        instanceDirectory.replace(path.resolve(this.directory), "."),
        instance.arguments,
        [path.basename(this.directory)],
        instance.dependsOn
      );

      if (!images.has(instancePath)) {
        images.add(instancePath);
        const dockerfile = path.resolve(instanceDirectory, "Dockerfile");
        fs.move(instanceSrcPath, instancePath, (error) => {
          if (error) {
            requestHandler.throwError(
              400,
              "bad request",
              "Invalid file format"
            )();
          } else {
            const docker = new Docker("openjdk:alpine", "/usr/src");
            docker.expose(22);
            docker.run("apk add openssh-server && ssh-keygen -A");
            docker.run(`echo 'root:${config.ssh.ssh_password}' | chpasswd`);
            docker.run(
              "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config"
            );
            docker.run(
              "sed -i 's/#PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config"
            );
            docker.copy(".", ".");
            docker.run('echo "Welcome to Boxit" > /etc/motd');
            docker.setEntrypoint("java", ["-jar", path.basename(instancePath)]);
            docker.write(dockerfile);
          }
        });
      }
    });
    return this.dockerCompose.write(
      path.resolve(this.directory, "docker-compose.yaml")
    );
  }

  enableSSH() {
    for (var service in this.dockerCompose.services) {
      this.dockerCompose.exec(service, "/usr/sbin/sshd -D", this.directory);
    }
  }

  up() {
    return this.dockerCompose.up(this.directory);
  }

  down() {
    return this.dockerCompose.down(this.directory);
  }
}

module.exports = Project;

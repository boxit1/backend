const fs = require("fs");
const RequestHandler = require("../utils/RequestHandler");
const Logger = require("../utils/logger");
const _ = require("lodash");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class Docker {
  constructor(image, workdir) {
    this.image = `FROM ${image}`;
    this.workdir = `WORKDIR ${workdir}`;
    this.steps = [];
  }

  copy(source, destination) {
    this.steps.push(`COPY ${source} ${destination}`);
  }

  run(command) {
    this.steps.push(`RUN ${command}`);
  }

  expose(port) {
    this.steps.push(`EXPOSE ${port}`);
  }

  setEntrypoint(command, args) {
    this.entrypoint = `ENTRYPOINT ["${command}"`;
    args.forEach((arg) => {
      this.entrypoint += `, "${arg}"`;
    });
    this.entrypoint += "]";
    return this;
  }

  write(destination) {
    if (_.isNull(this.image) || _.isUndefined(this.image)) {
      requestHandler.throwError(500, "internal error", "Image not defined")();
    }
    let result = `${this.image}\n${this.workdir}\n`;
    this.steps.forEach((step) => {
      result += `${step}\n`;
    });
    result += this.entrypoint;
    fs.writeFileSync(destination, result, (error) => {
      if (error) {
        requestHandler.throwError(500, "internal error", error.message)();
      }
    });
  }
}

module.exports = Docker;

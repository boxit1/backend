const jwt = require("jsonwebtoken");
const _ = require("lodash");
const config = require("../config/config");
const RequestHandler = require("./RequestHandler");
const Logger = require("./logger");
const Joi = require("joi");
const email = require("./email");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

function getTokenFromHeader(req) {
  if (
    (req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Token") ||
    (req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Bearer")
  ) {
    return req.headers.authorization.split(" ")[1];
  }

  return null;
}

function verifyToken(req, res, next) {
  if (config.app.single_user) {
    next();
  }
  else {
    try {
      if (_.isUndefined(req.headers.authorization)) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      }
  
      const Bearer = req.headers.authorization.split(" ")[0];
  
      if (!Bearer || Bearer !== "Bearer") {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      }
  
      const token = req.headers.authorization.split(" ")[1];
  
      if (!token) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      }
      jwt.verify(token, config.auth.jwt_secret, (err, decoded) => {
        if (err) {
          requestHandler.throwError(
            401,
            "Unauthorized",
            "please provide a valid token, your token might be expired"
          )();
        }
        req.decoded = decoded;
        next();
      });
    } catch (err) {
      requestHandler.sendError(req, res, err);
    }
  }
}

function verifyParamsToken(req, res, next) {
  try {
    if (_.isUndefined(req.params.token)) {
      requestHandler.throwError(
        401,
        "Unauthorized",
        "Not Authorized to access this resource!"
      )();
    }
    const token = req.params.token;
    if (!token) {
      requestHandler.throwError(
        401,
        "Unauthorized",
        "Not Authorized to access this resource!"
      )();
    }
    jwt.verify(token, config.auth.email_token_secret, (err, decoded) => {
      if (err) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "please provide a valid token, your token might be expired"
        )();
      }
      req.decoded = decoded;
      next();
    });
  } catch (err) {
    requestHandler.sendError(req, res, err);
  }
}

function getTokenFromParams(req) {
  const token = req.params.token;
  const schema = {
    token: Joi.string().required()
  };
  const { error } = Joi.validate(
    {
      token: token
    },
    schema
  );
  requestHandler.validateJoi(
    error,
    400,
    "bad Request",
    error ? error.details[0].message : ""
  );
  return req.params.token;
}

module.exports = {
  getJwtToken: getTokenFromHeader,
  getJwtTokenParams: getTokenFromParams,
  isAuthenticated: verifyToken,
  verifyParamsToken: verifyParamsToken
};

const Logger = require("./logger");
const fs = require("fs");
const AdmZip = require("adm-zip");
const { zip } = require("lodash");

module.exports = {
  unzipFile(fromPath, destinationPath) {
    new AdmZip(fromPath).extractAllTo(destinationPath);
  },

  zipDirectory(fromPath, destinationPath) {
    const zip = new AdmZip();
    zip.addLocalFolder(fromPath);
    zip.writeZip(destinationPath);
  },
};

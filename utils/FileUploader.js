const multer = require("multer");
const fs = require("fs");
const path = require("path");
const _ = require("lodash");
const uuid = require("uuid");

class FileUploader {
  constructor(destination, fieldName, limitSize = null, fileTypes = null) {
    this.destination = destination;
    if (!fs.existsSync(destination)) {
      fs.mkdirSync(destination);
    }

    this.limitSize = limitSize;
    this.fileTypes = fileTypes;

    this.storage = multer.diskStorage({
      destination: this.destination,
      filename: (req, file, cb) => {
        cb(null, `${uuid.v4()}${path.extname(file.originalname)}`);
      },
    });

    this.upload = multer({
      storage: this.storage,
      limits: { fileSize: limitSize },
      fileFilter: (req, file, cb) => {
        if (_.isNull(fileTypes)) {
          return cb(null, true);
        } else {
          const extname = fileTypes.test(
            path.extname(file.originalname).toLowerCase()
          );
          const mimetype = fileTypes.test(file.mimetype);
          if (mimetype && extname) {
            return cb(null, true);
          } else {
            cb({
              message: "Invalid file type",
            });
          }
        }
      },
    }).single(fieldName);
  }
}

module.exports = FileUploader;

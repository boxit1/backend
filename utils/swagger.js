const swaggerJsDoc = require("swagger-jsdoc");
const config = require("../config/config");
const { app } = require("../config/config");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Boxit API",
      description: "Boxit API Information",
      contact: {
        name: "Francisco Silva",
      },
      servers: [`http://localhost:${config.app.port}`],
    },
    host: `localhost:${config.app.port}`,
    schemes: ["http"],
    basePath: '/api/',
    securityDefinitions: {
			Bearer: {
				type: 'apiKey',
				description: 'JWT authorization of an API',
				name: 'Authorization',
				in: 'header',
			},
		},
    tags: [
      {
        name: "Authentication",
      },
      {
        name: "Users",
      },
      {
        name: "Projects",
      },
    ],
  },
  apis: ["./router/api/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

module.exports = swaggerDocs;

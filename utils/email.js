const config = require("../config/config");
const Logger = require("./logger");
const nodemailer = require("nodemailer");

const logger = new Logger();
let transporter = nodemailer.createTransport({
  service: config.email.service,
  auth: {
    user: config.email.user,
    pass: config.email.password
  }
});

module.exports = {
  sendEmail(fromEmail, toEmail, subject, textContent, htmlContent) {
    return new Promise((resolve, reject) => {
      transporter.sendMail(
        {
          from: fromEmail,
          to: toEmail,
          subject: subject,
          text: textContent,
          html: htmlContent
        },
        (error, info) => {
          if (error) {
            reject(error);
          }
          resolve(info);
        }
      );
    });
  }
};

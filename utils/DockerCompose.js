const yaml = require("js-yaml");
const fs = require("fs");
const _ = require("lodash");
const dockerCompose = require("docker-compose");
const path = require("path");
const Logger = require("./logger");
const RequestHandler = require("./RequestHandler");
const { exec } = require("child_process");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class DockerCompose {
  constructor(version) {
    this.version = version;
    this.services = {};
    this.networks = {};
  }

  state(container) {
    return new Promise((resolve, reject) => {
      exec(`docker inspect ${container}`, (error, stdout, stderr) => {
        if (error) {
          reject();
        }
        if (stderr) {
          reject();
        }
        const inspect = JSON.parse(stdout)[0];
        resolve(inspect);
      });
    });
  }

  exec(container, command, directory) {
    dockerCompose.exec(container, command, { cwd: directory }).then(
      () => {
        console.log("Done");
      },
      (err) => {
        //console.log("something went wrong:", err);
      }
    );
  }

  loadFile(sourceFile) {
    try {
      const doc = yaml.safeLoad(fs.readFileSync(sourceFile));
      this.version = doc.version;
      this.services = doc.services;
      this.networks = doc.networks;
    } catch (error) {
      requestHandler.throwError(400, "bad request", error.message)();
    }
  }

  addService(uuid, name, image, command, networks, dependsOn = null) {
    Object.assign(this.services, {
      [name]: {
        build: image,
        image: `${uuid}_${path.basename(image)}`,
        container_name: `${uuid}_${name}`,
        command: command.join(" "),
        stdin_open: true,
        tty: true,
        networks: networks,
      },
    });
    if (!_.isNull(dependsOn) && !_.isUndefined(dependsOn)) {
      Object.assign(this.services[name], {
        depends_on: dependsOn,
      });
    }
  }

  addNetworks(name) {
    Object.assign(this.networks, {
      [name]: {
        name: `${name}_network`,
      },
    });
  }

  write(destination) {
    const yamlStr = yaml.safeDump(this);
    return new Promise((resolve, reject) => {
      fs.writeFileSync(destination, yamlStr, (error) => {
        if (error) {
          requestHandler.throwError(400, "bad request", error.message)();
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  up(directory) {
    return dockerCompose.upAll({
      cwd: directory,
    });
  }

  down(directory) {
    return dockerCompose.down({
      cwd: directory,
      commandOptions: ["--rmi", "all"],
    });
  }
}

module.exports = DockerCompose;

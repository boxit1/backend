const execFile = require("child_process").execFile;
const exec = require("child_process").exec;

class BashConsole {
    runDockerPSFilterName(filter) {
        return new Promise((resolve, reject) => {
            execFile(
                "docker",
                ["ps", "-a", "-q", "-f", `name=${filter}`],
                (e, stdout, stderr) => {
                    if (e) {
                        reject();
                    }
                    if (stderr) {
                        reject();
                    }

                    var inspect = stdout.split("\n");
                    // console.log("inspect: ", inspect);
                    resolve(inspect.slice(0, inspect.length - 1));
                }
            );
        });
    }

    runDockerPSFilterId(filter) {
        return new Promise((resolve, reject) => {
            execFile(
                "docker",
                ["ps", "-a", "-f", `id=${filter}`],
                (e, stdout, stderr) => {
                    if (e) {
                        reject();
                    }
                    if (stderr) {
                        reject();
                    }

                    var inspect = stdout.split("\n");
                    // console.log("inspect: ", inspect);
                    resolve(inspect.slice(1, inspect.length - 1));
                }
            );
        });
    }

    runDockerInspect(container) {
        return new Promise((resolve, reject) => {
            exec(`docker inspect ${container}`, (error, stdout, stderr) => {
                if (error) {
                    reject();
                }
                if (stderr) {
                    reject();
                }
                const inspect = JSON.parse(stdout)[0];
                resolve(inspect);
            });
        });
    }

    async getContainerInfo(containerUUID) {
        // console.log(containerUUID);
        // obter o output do comando do docker para retirar o nome
        let consoleOut = await this.runDockerPSFilterName(containerUUID).then(
            (r) => {
                return r;
            }
        );
        // console.log(consoleOut);

        let states = [];
        if (consoleOut.length !== 0) {
            // obter o nome dos containers
            // let regex = /   +/g;
            // // console.log("consoleOut: ", consoleOut);
            // let containerNames = consoleOut.map((element) => {
            //     let all = element.split(regex);
            //     return all[all.length - 1];
            // });
    
            // console.log("containerNames :", containerNames);
    
            // Obter a info dos containers
            let dockerInspect = [];
            for (const key in consoleOut) {
                if (Object.hasOwnProperty.call(consoleOut, key)) {
                    const containerName = consoleOut[key];
                    dockerInspect.push(this.runDockerInspect(containerName));
                }
            }
    
            // console.log("dockerInspect: ", dockerInspect);
            await Promise.all(dockerInspect)
                .then((instanceState) => {
                    // console.log(
                    //     "Promise-instanceState: ",
                    //     JSON.stringify(instanceState)
                    // );
                    states.push(instanceState);
                })
                .catch((err) => console.log(err));
            // console.log("states: ", states);
        }else{
            states = [[{containerUUID}]]
        }

        return states;
    }


    dockerDown(container){
        return new Promise((resolve, reject) => {
            execFile(
                "docker",
                ["stop", `${container}`],
                (e, stdout, stderr) => {
                    if (e) {
                        reject();
                    }
                    if (stderr) {
                        reject();
                    }

                    resolve(stdout);
                }
            );
        });
    }

    dockerUp(container){
        return new Promise((resolve, reject) => {
            execFile(
                "docker",
                ["start", `${container}`],
                (e, stdout, stderr) => {
                    if (e) {
                        reject();
                    }
                    if (stderr) {
                        reject();
                    }

                    resolve(stdout);
                }
            );
        });
    }
}

module.exports = BashConsole;

// let _console = new BashConsole

// main()

// execFile('ls', ['-ltr'], (e, stdout, stderr) => {
//     if (e instanceof Error) {
//         console.error(e)
//         throw e;
//     }

//     console.log('stdout ', stdout);
//     console.log('stderr ', stderr);
// })

// execFile('docker', ['ps','-f','name=30656152-dc55-4c17-9c9a-2184c3466e8b'], (e, stdout, stderr) => {
//     if (e instanceof Error) {
//         console.error(e)
//         throw e;
//     }

//     console.log('stdout :\n', stdout);
//     console.log('stderr :\n', stderr);
// })

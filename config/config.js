require("dotenv").config();

module.exports = {
  app: {
    port: parseInt(process.env.DEV_APP_PORT),
    appName: process.env.APP_NAME,
    env: process.env.NODE_ENV,
    single_user: process.env.SINGLE_USER == "true",
    frontend_url: process.env.FRONTEND_URL
  },
  ssh: {
    ssh_password: process.env.SSH_PASSWORD,
  },
  logger: {
    info: process.env.LOG_INFO == "true",
    error: process.env.LOG_ERROR == "true",
    warn: process.env.LOG_WARN == "true",
  },
  auth: {
    jwt_secret: process.env.JWT_SECRET,
    jwt_expires_in: process.env.JWT_EXPIRES_IN,
    algorithm: process.env.JWT_ALGORITHM,
    saltRounds: parseInt(process.env.SALT_ROUND),
    refresh_token_secret: process.env.REFRESH_TOKEN_SECRET,
    refresh_token_expires_in: process.env.REFRESH_TOKEN_EXPIRES_IN, // need to be greater than the jwt_expire_in
    email_token_secret: process.env.EMAIL_TOKEN_SECRET,
    email_token_expires_in: process.env.EMAIL_TOKEN_EXPIRES_IN,
  },
  email: {
    service: process.env.EMAIL_SERVICE, // list in nodemailer well-known services, if using gmail, set less secure app access to true
    user: process.env.EMAIL_USER,
    password: process.env.EMAIL_PASSWORD,
    from: '"'
      .concat(process.env.EMAIL_NAME)
      .concat('" <')
      .concat(process.env.EMAIL_USER)
      .concat(">"),
  },
};

const Joi = require("joi");
const _ = require("lodash");
const BaseController = require("./BaseController");
const RequestHandler = require("../utils/RequestHandler");
const Logger = require("../utils/logger");
const unzip = require("../utils/unzip");
const path = require("path");
const fs = require("fs-extra");
const Docker = require("../utils/Docker");
const DockerCompose = require("../utils/DockerCompose");
const FileUploader = require("../utils/FileUploader");
const Project = require("../utils/Project");
const config = require("../config/config");
const BashConsole = require("../utils/BashConsole");

const fileUploader = new FileUploader("./uploads/", "project", null, /zip/);
const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class ProjectsController extends BaseController {
  static async getProjects(req, res) {
    try {
      const options = {
        include: [{ all: true }],
      };
      let result = await super.getList(req, "Projects", options);
      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        result = result.filter(
          (project) =>
            project.dataValues.users.filter(
              (user) => user.email == payload.email
            ).length !== 0
        );
      }
      result.map((proj) => {
        delete proj.dataValues.users;
        // const project = new Project(
        //   `./projects/${proj.dataValues.uuid}`,
        //   proj.dataValues.uuid
        // );
        // const states = project.getState();
        // Promise.all(states)
        //   .then((instances) => {
        //     const containers = instances.map((instance) => {
        //       return {
        //         name: instance.Name,
        //         state: instance.State.Status,
        //         ip:
        //           instance.NetworkSettings.Networks[
        //             `${proj.dataValues.uuid}_network`
        //           ].IPAddress,
        //       };
        //     });
        //     proj.dataValues.instances = containers;
        //     // project.dataValues.instances = containers;
        //     // return requestHandler.sendSuccess(
        //     //   res,
        //     //   "Project Imported Successfully",
        //     //   200
        //     // )({ result });
        //   })
        //   .catch((error) => {
        //     console.log(error);
        //   });
      });
      //console.log(result);
      return requestHandler.sendSuccess(res, "Projects Extracted")({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async addProject(req, res) {
    fileUploader.upload(req, res, async (error) => {
      try {
        if (error) {
          requestHandler.throwError(400, "bad request", error.message)();
        } else {
          // console.log(req.decoded.payload.email);
          const file = req.file;
          const instances = req.body.instances;

          const projectName = path.parse(file.filename).name;
          const projectDirectory = `./projects/${projectName}`;

          unzip.unzipFile(file.path, projectDirectory);
          fs.remove(file.path, (err) => {
            if (err) {
              requestHandler.throwError(400, "bad request", err.message)();
            }
          });

          let newProject = {
            uuid: projectName,
            name: path.parse(file.originalname).name,
            createdAt: new Date(),
            updatedAt: new Date(),
          };

          const result = await super.create(req, "Projects", newProject);
          if (!config.app.single_user) {
            const payload = req.decoded.payload;
            result.setUsers([payload.email]);
          }
          if (_.isNull(result)) {
            requestHandler.throwError(
              422,
              "Unprocessable Entity",
              "unable to process the contained instructions"
            )();
          }

          const project = new Project(projectName, projectDirectory, instances);
          project.build(instances).then(
            project.up().then(
              () => {
                project.enableSSH();
                const states = project.getState();
                Promise.all(states)
                  .then((instances) => {
                    const containers = instances.map((instance) => {
                      return {
                        name: instance.Name.replace(`/${projectName}_`, ""),
                        state: instance.State.Status,
                        ip:
                          instance.NetworkSettings.Networks[
                            `${projectName}_network`
                          ].IPAddress,
                      };
                    });
                    result.dataValues.instances = containers;
                    return requestHandler.sendSuccess(
                      res,
                      "Project Imported Successfully",
                      200
                    )({ result });
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              },
              (err) => {
                requestHandler.throwError(400, "bad request", err.message)();
              }
            )
          );
        }
      } catch (error) {
        return requestHandler.sendError(req, res, error);
      }
    });
  }

  static async sendProject(req, res) {
    try {
      const reqParam = req.params.uuid;
      const schema = {
        uuid: Joi.string(),
      };

      const { error } = Joi.validate({ uuid: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Project uuid"
      );

      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        const options = {
          where: {
            uuid: reqParam,
          },
          include: [{ all: true }],
        };

        const project = await super.getByCustomWhere(req, "Projects", options);
        //console.log(project);
        if (
          project.dataValues.users.filter(
            (user) => user.dataValues.email == payload.email
          ).length === 0
        ) {
          requestHandler.throwError(
            401,
            "Unauthorized",
            "Not Authorized to access this resource!"
          )();
        }
      }

      const projectDirectory = `./projects/${reqParam}/`;
      const file = `./downloads/${reqParam}.zip`;

      unzip.zipDirectory(projectDirectory, file);

      return requestHandler.sendSuccessFile(res)({
        path: file,
        name: `${reqParam}.zip`,
      });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async deleteProject(req, res) {
    try {
      const reqParam = req.params.uuid;
      const schema = {
        uuid: Joi.string(),
      };

      const { error } = Joi.validate({ uuid: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Project uuid"
      );

      let result;
      if (config.app.single_user) {
        result = await super.deleteByCustomWhere(req, "Projects", {
          uuid: reqParam,
        });
      } else {
        const payload = req.decoded.payload;
        const options = {
          where: { uuid: reqParam },
          include: [{ all: true }],
        };
        result = await super.getByCustomWhere(req, "Projects", options);

        if (
          result.dataValues.users.filter((user) => user.email == payload.email)
            .length !== 0
        ) {
          result = await super.deleteByCustomWhere(req, "Projects", {
            uuid: reqParam,
          });
        } else {
          requestHandler.throwError(
            401,
            "Unauthorized",
            "Not Authorized to access this resource!"
          )();
        }
      }

      const project = new Project(
        reqParam,
        path.resolve("projects/", reqParam)
      );
      project.load();
      project.down().then(
        () => {
          fs.remove(project.directory, (err) => {
            if (err) {
              requestHandler.throwError(400, "bad request", err.message)();
            } else {
              return requestHandler.sendSuccess(
                res,
                "Project Deleted Successfully"
              )({ result });
            }
          });
        },
        (err) => {
          requestHandler.throwError(400, "bad request", err.message)();
        }
      );
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }


  static async getProjectsState(req, res) {
    try {
      const options = {
        include: [{ all: true }],
      };
      const reqParam = req.params.uuid;
      const schema = {
        uuid: Joi.string(),
      };

      // console.log("reqParam: ", reqParam);

      const { error } = Joi.validate({ uuid: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Project uuid"
      );


      let dbResult = await super.getList(req, "Projects", options);
      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        dbResult = dbResult.filter(
          (project) =>
            project.dataValues.users.filter(
              (user) => user.email == payload.email
            ).length !== 0
        );
      }
      var projectsUUIDS = dbResult.map(proj => proj.dataValues.uuid);

      if (reqParam != undefined && projectsUUIDS.includes(reqParam)) {
        projectsUUIDS = [reqParam]
      } else if (reqParam != undefined && !projectsUUIDS.includes(reqParam)) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      } 

      // console.log(projectsUUIDS);
      let _console = new BashConsole

      let allContainersInfo = []
      projectsUUIDS.forEach(uuid => {
        allContainersInfo.push(_console.getContainerInfo(uuid))
      });

      // console.log("allContainersInfo: ", allContainersInfo);

      Promise.all(allContainersInfo)
      .then(instances => {
          // Ir buscar todos os projetos desejados iterando 
          // pela informação que cheogu pelo docker
          // console.log("instances: ", instances);
          const projects = instances.map((instance,i) => {
            // console.log("instance[0]: ", instance[0]);

            if (instance[0].length != 0) {
              
              // so o prejeto esta parado (nao obtem informação do docker)
              var project
              if (instance[0][0].containerUUID == undefined) {
                
                // get project uuid atravez do nome de uma instancia
                const projectUUID = instance[0][0].Name.substring(1,37)
                // console.log("projectUUID: ", projectUUID);
                
                // iterar por todas as instancias de cada projeto para nao se enviar
                // a informação toda (popar data)
                var allRunning = true
                const projectInstances = instance[0].map(i=>{
                  if(i.State.Status != 'running') {
                    allRunning = false
                  }  
                  return {
                    id: i.Id,
                    name: i.Name.replace(`/${projectUUID}_`, ""),
                    created: i.Created,
                    state: i.State.Status,
                    ip: i.NetworkSettings.Networks[`${projectUUID}_network`].IPAddress
                  }
                }) 
                
                // console.log("projectInstances: ", projectInstances);
    
                project = {
                  projectName: dbResult[i].name,
                  projectUUID: projectUUID,
                  createdAt: dbResult[i].createdAt,
                  updatedAt: dbResult[i].updatedAt,
                  state: allRunning ? 'running' : 'other',
                  instances: projectInstances
                }
              }else{
                project = {
                  projectName: dbResult[i].name,
                  projectUUID: instance[0][0].containerUUID,
                  createdAt: dbResult[i].createdAt,
                  updatedAt: dbResult[i].updatedAt,
                  state: "stoped",
                  instances: {}
                }
              }
                
              return project
            } 

            return {}
          });
          
          // console.log("PROJECTSS: ", projects);


          return requestHandler.sendSuccess(
            res,
            "Projects Info Extracted",
            200
          )({ projects });

        })
        .catch(error =>
          console.log(error)
        )

	  } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async down(req, res) {
    try {
      const reqParam = req.params.uuid;
      const schema = {
        uuid: Joi.string(),
      };

      const { error } = Joi.validate({ uuid: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Project uuid"
      );

      const options = {
        where: { uuid: reqParam },
        include: [{ all: true }],
      };
      let  result = await super.getByCustomWhere(req, "Projects", options);
    
      if (!config.app.single_user) {        
        const payload = req.decoded.payload;
      
        if (
          result.dataValues.users.filter((user) => user.email == payload.email)
            .length === 0
        ) {
          requestHandler.throwError(
            401,
            "Unauthorized",
            "Not Authorized to access this resource!"
          )();
        }
      }

      const project = new Project(
        reqParam,
        path.resolve("projects/", reqParam)
      );
      project.load();
      project.down().then(() => {
          delete result.dataValues.users
          return requestHandler.sendSuccess(
            res,
            "Project Stoped Successfully"
          )({ result });
        },
        (err) => {
          requestHandler.throwError(400, "bad request", err.message)();
        }
      );
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async up(req, res) {
    try {
      const reqParam = req.params.uuid;
      const schema = {
        uuid: Joi.string(),
      };

      const { error } = Joi.validate({ uuid: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Project uuid"
      );

      const options = {
          where: { uuid: reqParam },
          include: [{ all: true }],
      };
      let  result = await super.getByCustomWhere(req, "Projects", options);
     
      if (!config.app.single_user) {        
        const payload = req.decoded.payload;

        if (
          result.dataValues.users.filter((user) => user.email == payload.email)
            .length === 0
        ) {
          requestHandler.throwError(
            401,
            "Unauthorized",
            "Not Authorized to access this resource!"
          )();
        }
      }
      // console.log(result.users);
      // delete result.users
      // console.log(result.users);

      const project = new Project(
        reqParam,
        path.resolve("projects/", reqParam)
      );
      project.load();
      project.up().then(() => {
      delete result.dataValues.users
        return requestHandler.sendSuccess(
            res,
            "Project Started Successfully"
          )({ result });
        },
        (err) => {
          requestHandler.throwError(400, "bad request", err.message)();
        }
      );
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  






  static async instanceDown(req, res) {
    try {
      const reqParam = req.params.id;
      console.log();
      const schema = {
        id: Joi.string().hex().min(64).max(64),
      };

      const { error } = Joi.validate({ id: reqParam }, schema);
      // console.log("Error: ", error);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Instance id"
      );
      
      const options = {
        include: [{ all: true }],
      };
      let dbResult = await super.getList(req, "Projects", options);
      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        dbResult = dbResult.filter(
          (project) =>
            project.dataValues.users.filter(
              (user) => user.email == payload.email
            ).length !== 0
        );
      }
      // console.log("dbResult: ", dbResult);

      var projectsUUIDS = dbResult.map(proj => proj.dataValues.uuid);

      let _console = new BashConsole
      let consoleOut = await _console.runDockerPSFilterId(reqParam).then(
        (r) => {
            return r;
        }
      );

      // console.log("consoleOut: ", consoleOut);
      let containerName
      if (consoleOut.length !== 0) {
        let regex = /   +/g;
        containerName = consoleOut.map((element) => {
            let all = element.split(regex);
            return all[all.length - 1];
        });
      }

      let hasAccess = false
      if(containerName != undefined){
        projectsUUIDS.every(e=>{
          if(containerName[0].includes(e)){
            hasAccess = true
          }
          return !hasAccess
        })
      }

      if (!hasAccess) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      } else{
        _console.dockerDown(reqParam).then(r=>{
          return requestHandler.sendSuccess(
            res,
            "Instance Stoped Successfully"
          )({ 
              instanceID: r,
              fromProject: containerName[0].substr(0,containerName[0].indexOf("_")),
            });
        },
        (err) => {
          requestHandler.throwError(400, "bad request", err.message)();
        })      
      }
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  
  






  static async instanceUp(req, res) {
    try {
      const reqParam = req.params.id;
      console.log();
      const schema = {
        id: Joi.string().hex().min(64).max(64),
      };

      const { error } = Joi.validate({ id: reqParam }, schema);
      // console.log("Error: ", error);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid Instance id"
      );
      
      const options = {
        include: [{ all: true }],
      };
      let dbResult = await super.getList(req, "Projects", options);
      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        dbResult = dbResult.filter(
          (project) =>
            project.dataValues.users.filter(
              (user) => user.email == payload.email
            ).length !== 0
        );
      }
      // console.log("dbResult: ", dbResult);

      var projectsUUIDS = dbResult.map(proj => proj.dataValues.uuid);

      let _console = new BashConsole
      let consoleOut = await _console.runDockerPSFilterId(reqParam).then(
        (r) => {
            return r;
        }
      );

      // console.log("consoleOut: ", consoleOut);
      let containerName
      if (consoleOut.length !== 0) {
        let regex = /   +/g;
        containerName = consoleOut.map((element) => {
            let all = element.split(regex);
            return all[all.length - 1];
        });
      }

      let hasAccess = false
      if(containerName != undefined){
        projectsUUIDS.every(e=>{
          if(containerName[0].includes(e)){
            hasAccess = true
          }
          return !hasAccess
        })
      }

      if (!hasAccess) {
        requestHandler.throwError(
          401,
          "Unauthorized",
          "Not Authorized to access this resource!"
        )();
      } else{
        _console.dockerUp(reqParam).then(r=>{
          return requestHandler.sendSuccess(
            res,
            "Instance Started Successfully"
            )({ 
              instanceID: r,
              fromProject: containerName[0].substr(0,containerName[0].indexOf("_")),
            });
        },
        (err) => {
          requestHandler.throwError(400, "bad request", err.message)();
        })      
      }
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = ProjectsController;

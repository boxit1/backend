const Joi = require("joi");
const _ = require("lodash");
const BaseController = require("./BaseController");
const RequestHandler = require("../utils/RequestHandler");
const Logger = require("../utils/logger");
const config = require("../config/config");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class UsersController extends BaseController {
  static async getUserByPk(req, res) {
    try {
      const reqParam = req.params.email;
      const schema = {
        email: Joi.string().email().required(),
      };

      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        if (payload.email !== reqParam) {
          requestHandler.throwError(
            400,
            "bad request",
            "invalid email address"
          )();
        }
      }

      const { error } = Joi.validate({ email: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid User email"
      );
      req.params.pk = reqParam;
      const result = await super.getByPk(req, "Users");
      delete result.dataValues.passwordHash;
      return requestHandler.sendSuccess(res, "User Data Extracted")({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async deleteByPk(req, res) {
    try {
      const reqParam = req.params.email;

      const schema = {
        email: Joi.string().email(),
      };

      if (!config.app.single_user) {
        const payload = req.decoded.payload;
        if (payload.email !== reqParam) {
          requestHandler.throwError(
            400,
            "bad request",
            "invalid email address"
          )();
        }
      }

      const { error } = Joi.validate({ email: reqParam }, schema);
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        "invalid User email"
      );

      req.params.pk = reqParam;
      const result = await super.deleteByCustomWhere(req, "Users", {
        email: reqParam,
      });
      delete result.dataValues.passwordHash;
      return requestHandler.sendSuccess(
        res,
        "User Deleted Successfully"
      )({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = UsersController;

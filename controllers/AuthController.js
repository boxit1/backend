const Joi = require("joi");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const _ = require("lodash");
const jwt = require("jsonwebtoken");
const RequestHandler = require("../utils/RequestHandler");
const Logger = require("../utils/logger");
const BaseController = require("../controllers/BaseController");
const email = require("../utils/email");
const config = require("../config/config");
const auth = require("../utils/auth");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);
const tokenList = {};

class AuthController extends BaseController {
  static async login(req, res) {
    try {
      // Validating the request
      const schema = {
        email: Joi.string().email().required(),
        password: Joi.string().required(),
      };
      const { error } = Joi.validate(
        {
          email: req.body.email,
          password: req.body.password,
        },
        schema
      );
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        error ? error.details[0].message : ""
      );

      // Check if user exists
      const options = {
        where: {
          email: crypto
            .createHash("sha256")
            .update(req.body.email)
            .digest("base64"),
        },
      };
      const user = await super.getByCustomWhere(req, "Users", options);
      if (!user) {
        requestHandler.throwError(
          400,
          "bad request",
          "email not found"
        )();
      }

      // Check if user account is active
      if (user.accountStatus !== "active") {
        requestHandler.throwError(
          400,
          "bad request",
          "your account is not active"
        )();
      }

      // Compare password from request with password on database
      await bcrypt.compare(req.body.password, user.passwordHash).then(
        requestHandler.throwIf(
          (r) => !r,
          400,
          "incorrect",
          "failed to login, bad credentials"
        ),
        requestHandler.throwError(500, "bcrypt error")
      );

      // Update lastLogin date
      const data = {
        lastLogin: new Date(),
      };
      await super.updateByCustomWhere(req, "Users", data, options);

      // Creating the token
      const payload = _.omit(user.dataValues, [
        "createdAt",
        "updatedAt",
        "lastLogin",
        "passwordHash",
        "accountStatus",
        "role",
      ]);

      const token = jwt.sign({ payload }, config.auth.jwt_secret, {
        expiresIn: config.auth.jwt_expires_in,
        algorithm: config.auth.algorithm,
      });

      // Creating the refresh token
      const refreshToken = jwt.sign(
        { payload },
        config.auth.refresh_token_secret,
        { expiresIn: config.auth.refresh_token_expires_in }
      );

      // Creating the request response
      const response = {
        status: "Logged in",
        token,
        refreshToken,
      };

      // Adding the token and refresh token to the list
      tokenList[refreshToken] = response;

      // Getting Users Name
      var userName = user.firstName;

      // Sending the response
      requestHandler.sendSuccess(
        res,
        "User logged in Successfully"
      )({ userName, token, refreshToken });
    } catch (error) {
      requestHandler.sendError(req, res, error);
    }
  }

  static async signUp(req, res) {
    try {
      // Validating the request
      const data = req.body;
      const schema = {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        role: Joi.string().required(),
      };
      const { error } = Joi.validate(
        {
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          password: data.password,
          role: data.role,
        },
        schema
      );
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        error ? error.details[0].message : ""
      );

      const userEmail = crypto
        .createHash("sha256")
        .update(data.email)
        .digest("base64");
      // Check if exist any user with the request email
      const options = {
        where: {
          email: userEmail,
        },
      };
      const user = await super.getByCustomWhere(req, "Users", options);
      if (user) {
        requestHandler.throwError(
          400,
          "bad request",
          "invalid email account, email already existed"
        )();
      }

      // Hashing the password
      const passwordHash = bcrypt.hashSync(
        data.password,
        config.auth.saltRounds
      );

      // Creating the user object
      const newUser = {
        firstName: data.firstName,
        lastName: data.lastName,
        email: userEmail,
        passwordHash: passwordHash,
        accountStatus: "verifying",
        role: data.role,
        lastLogin: null,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      // Insert the object in the database
      const createdUser = await super.create(req, "Users", newUser);
      if (!_.isNull(createdUser)) {
        // Creating the token payload
        const payload = _.omit(newUser, [
          "createdAt",
          "updatedAt",
          "lastLogin",
          "passwordHash",
          "accountStatus",
          "role",
        ]);

        // Send the verification with the token
        const token = jwt.sign({ payload }, config.auth.email_token_secret, {
          expiresIn: config.auth.email_token_expires_in,
          algorithm: config.auth.algorithm,
        });

        const verificationLink = `${config.app.frontend_url}/api/verifyAccount/${token}`;
        await email.sendEmail(
          config.email.from,
          data.email,
          "Boxit Registration",
          "",
          `<form method="POST" action="${verificationLink}">
            <h2>You have register into Boxit</h2>
            </br>
            <p>Please press the following button to activate your account:</p>
            <button type="submit">Verify Account</button>
          </form>`
        );

        // Respond to the request
        requestHandler.sendSuccess(
          res,
          "activation email sent successfully",
          201
        )();
      } else {
        requestHandler.throwError(
          422,
          "Unprocessable Entity",
          "unable to process the contained instructions"
        )();
      }
    } catch (error) {
      requestHandler.sendError(req, res, error);
    }
  }

  static async verifyAccount(req, res) {
    try {
      // Get the token payload
      const payload = req.decoded.payload;
      // Get the user to verify
      const options = {
        where: {
          email: payload.email,
        },
      };
      const user = await super.getByCustomWhere(req, "Users", options);
      // Check if the user exists
      if (!user) {
        requestHandler.throwError(
          400,
          "bad request",
          "invalid email address"
        )();
      }

      // Check if the account status of the user is not active
      if (user.accountStatus === "active") {
        requestHandler.throwError(
          400,
          "bad request",
          "account already activated"
        )();
      }

      // Set the user account status to active
      const data = {
        accountStatus: "active",
      };
      await super.updateByCustomWhere(req, "Users", data, options);

      // Respond to the request
      // Estou a responder aqui para puder enviar html com a resposta sem complicar o resto do codigo
      // copiei o sendSuccess para qui

      logger.log(
        `a request has been made and proccessed successfully at: ${new Date()}`,
        "info"
      );
      res.setHeader('Content-type','text/html')
      res.status(200).send(
              `<div style="display: flex; justify-content: center; align-items: center; height: 90vh; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',    sans-serif;">
              <h2 style="text-align: center;">Account&nbsp;activated&nbsp;successfully<br/>You may now 
                <a href="${config.app.frontend_url}/login">Login</a> in BoxIt</h2>
              </div>`
              );
      

      // Respond to the request
      //requestHandler.sendSuccess(res, "Account activated successfully", 201)();   
    } catch (error) {

      // A mesma coisa que o de cima
      logger.log(
        `Error during processing request: ${`${req.protocol}://${req.get(
          "host"
        )}${req.originalUrl}`} details message: ${error.message}`,
        "error"
      );
      res.setHeader('Content-type','text/html')
      res.status(500).send(`
        <div style="display: flex; justify-content: center; align-items: center; height: 90vh; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',    sans-serif;">
          <h2 style="text-align: center;">Account already activated</h2>
        </div>
      `)
      
      // requestHandler.sendError(req, res, error);
    }
  }

  static async refreshToken(req, res) {
    try {
      // Validating the request
      const data = req.body;
      if (_.isNull(data)) {
        requestHandler.throwError(
          400,
          "bad request",
          "please provide the refresh token in request body"
        )();
      }
      const schema = {
        refreshToken: Joi.string().required(),
      };
      const { error } = Joi.validate(
        { refreshToken: req.body.refreshToken },
        schema
      );
      requestHandler.validateJoi(
        error,
        400,
        "bad Request",
        error ? error.details[0].message : ""
      );

      // Getting the token form the header
      const tokenFromHeader = auth.getJwtToken(req);

      // Getting the payload of the token
      const user = jwt.decode(tokenFromHeader);

      // Check if the refresh token exist
      if (data.refreshToken && data.refreshToken in tokenList) {
        // Creating a new token
        const payload = user.payload
        const token = jwt.sign({ payload }, config.auth.jwt_secret, {
          expiresIn: config.auth.jwt_expires_in,
          algorithm: config.auth.algorithm,
        });

        // Creating the response with the new token
        const response = {
          token,
        };

        // Updating the token list
        tokenList[data.refreshToken].token = token;

        // Sending the response
        requestHandler.sendSuccess(
          res,
          "a new token is issued ",
          200
        )(response);
      } else {
        requestHandler.throwError(
          400,
          "bad request",
          "no refresh token present in refresh token list"
        )();
      }
    } catch (err) {
      requestHandler.sendError(req, res, err);
    }
  }

  static async logOut(req, res) {
    try {
      // Remove the headers token needs to be handle on the frontend
      requestHandler.sendSuccess(res, "User Logged Out Successfully")();
    } catch (error) {
      requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = AuthController;
